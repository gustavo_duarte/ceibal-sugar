��    R      �  m   <      �     �                              %     .     @     G     N  
   S  
   ^     i     m     t     {     �     �     �     �     �  3   �     #     0     5     ;     A     P     X  -   a     �     �     �     �     �  
   �     �     �     �     �  	   �  
   �  	   �  	   �     	     	     	     *	     B	     P	     X	     `	  $   h	     �	     �	     �	     �	     �	     �	     �	     �	     �	  *   �	      %
     F
     L
  !   U
     w
     ~
  
   �
  	   �
     �
     �
  "   �
     �
     �
     �
                +  �  9     �     �     �     �     �     �  	   �     �               +     4     :     J     P     X     ^  $   q      �      �     �     �  4   �     #     /     4     :     B     Q     ]  +   d     �     �     �     �     �  	   �     �     �     �     �               1     J     _     m      �  )   �     �     �     �     �  )   	     3     I  
   Y     d     t     �     �     �     �  ,   �  $   �               $     8     G     Y     l     �     �     �     �     �  $   �  .        4     I         (   E   5   F   G       
      P                              "          !       >      K   #   7         J           L   I   /      :       6   .   %         -              R       ;          D   A   )      9   *      8      H   N   4          3   ?   0          $                   &          '   	       1   <              +   C          B      ,   Q   @   O                M   2          =        %(free_space)d MB Free %d KB %dB %dKB %dMB <Ctrl>%d About Me About my Computer Accept Anyone Back Background Backup URL Box Build: Cancel Cancel changes Cannot connect to the server. Click to change color: Click to change your color: Close Collaboration Could not access ~/.i18n. Create standard settings. Description: Done Erase Frame IP address: %s Journal Language Language for code=%s could not be determined. Later Logout Mute My Settings My class My friends Name: Neighborhood Next Open Open with Past month Past week Past year Power Register Registration Failed Registration Successful Remove friend Removed Send to Server: Setting for muting the sound device. Show Journal Show contents Shutdown Since yesterday Sorry I do not speak '%s'. Sound Muted Start Stop Sugar: The server could not complete the request. Timezone setting for the system. Today Triangle URL where the backup is saved to. Unmute Untitled User Color User Name View Details View Source Volume level for the sound device. Warning Wireless You must enter a name. Your Journal is empty Your Journal is full instantaneous Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-08-30 20:01+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: mg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Pootle 2.0.5
 %(free_space)d MB Malalaka %d KB %dB %dKB %dMB <Ctrl>%d Momba ahy Ny momba ny solosaina Ekeo Ny olona rehetra Miverina Afara Tahirizo ny URL Boaty Aoreno: Foano Foano ny fanovàna Tsy afaka mifandray amin'ny mpizara. Tsindrio raha te hanova ny loko: Tsindrio raha hanova ny lokonao: Akatony Fiaraha-miasa Tsy afaka niditra ~/.i18n. Mamorona tefy marofibika. Mombamomba: Vita Soloy Faritra Adiresy IP: %s Firaiketana Fiteny Tsy fantatra ny fitenin'ny sora-drindra=%s. Amin'ny manaraka Miala sehatra Esory ny feo Ireo tefiko Ny kilasiko Ny namako Anarana: Ny manodidina Manaraka Sokafy Sokafy miaraka amin'ny Tamin'ny iray volana Tamin'ny herinandro lasa Tamin'ny herin-taona Hery, Tanjaka Voasoratra anarana Nisy olana ny fisoratana anarana Vita soa aman-tsara ny fisoratana anarana Esory io namana io Esory Alefaso any amin'i Mpamatsy, Mpizara Tefy fanafoanana ny feon'ny fitaovam-peo. Asehoy ny firaiketana Asehoy ny atiny Pio, Vonoy Nanomboka omaly Azafady izaho tsy miteny '%s'. Nesorina ny feo Atomboy Ajanony Sugar: Tsy afaka mameno ny fangatahana ny mpamatsy. Tefin'ny faritrora ho an'ny rafitra. Anio, Androany Telozoro URL misy ny tahiry. Avereno ny feo Tsy misy lohateny Lokon'ny mpampiasa Anaran'ny mpampiasa Jereo ny antsipiriany Asehoy ny sora-drindra Fanovàna ny hadirim-peo. Tandremo Tsy misy taroby Tsy maintsy mampiditra anarana ianao Tsy misy n'inoninona ao anatin'ny firaiketanao Feno ny firaiketanao eo no ho eo 