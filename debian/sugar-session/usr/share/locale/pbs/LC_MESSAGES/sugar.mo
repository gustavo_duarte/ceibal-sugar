��    8      �  O   �      �     �     �     �  3        A     H     W     o     �     �     �     �  E   �       +   +  $   W  #   |     �  -   �     �  	                  &     /     C     K     Y  	   a     k     �     �     �     �     �     �     �  ^   �     7     I  �   P     �  0   �     %	  	   4	     >	     G	  4   Z	     �	  v   �	     
     9
     F
     `
     m
  �  �
       '     !   >  0   `     �     �     �     �     �     �  2        7     S     o  O   {  1   �  %   �  &   #  <   J  #   �     �     �     �     �     �               -     D  !   S     u     }  
   �     �     �     �     �  x   �     R     g  u   z  %   �  <        S     i  
         �  M   �     �  t         �     �     �     �     �        !       4       1   $   8   7                                     ,   )       3   *   .          (                            '   
      5           /                                    6   "                   +   &   -       #      2              	       0   %                     About Me About my Computer Access Point Name (APN): Automatic power management (increases battery life) Build: Checking %s... Checking for updates... Click to change your color: Collaboration Copyright and License Date & Time Discard network history Discard network history if you have trouble connecting to the network Downloading %s... Error in automatic pm argument, use on/off. Error in specified argument use 0/1. Error in specified color modifiers. Error in specified colors. Error in specified radio argument use on/off. Error timezone does not exist. Firmware: Frame Full license: Identity Modem Configuration Network Not available Number: Password: Personal Identity Number (PIN): Power Power management Radio Serial Number: Server: Software Software Update Software updates correct errors, eliminate security vulnerabilities, and provide new features. State is unknown. Sugar: The server is the equivalent of what room you are in; people on the same server will be able to see each other, even when they aren't on the same network. Timezone Turn off the wireless radio to save battery life Updating %s... Username: Wireless Wireless Firmware: You can install %s update You can install %s updates You must enter a name. You will need to provide the following information to set up a mobile broadband connection to a cellular (3G) network. Your software is up-to-date fill:     %s fill:     color=%s hue=%s stroke:   %s stroke:   color=%s hue=%s Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-08-30 23:02+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: pbs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.0.5
 kauk má ker se lumeí Ngube'eí nda'ets' kily'e ngunjiu´pu ndanú kunjuei (APN): makju liji ngun´iang (majaixp manup ngun´iang) Mamat mats´aut %s... mata´au viñkjiut... matees kjup mavaun' re xikjia' tibiáik Ngu'uix mayn' kutau ne katsjau Nqbe'en kunju'up se kuan mayn' pep kuljeing kunju' vixa´aun re mjang ne nikji vixa´aun re mjang ne nikji lupup %s... ndich´u ne lik´iajam mep eje lich´jaú liji ne manaja ki´it maleiñ/ mavung ndich´u ne lik´iajam vep´eje´make´ki´it 0/1 Ndich'u re se ndavaun' d-ajap xikjia' Ndich'u re se ndavaun' d-ajap  xikjia' ndichu ne lik´iajam ve´eje´make´ngube´ei ki´it on/off. Ndich'u kauts' se tibiaí nip-limí Lem puse vupa'i Kingye'ep lumeí mú Lipia rik'ix Licencia = Rik'ix Vumau' pep m'us KUE´E MATSSAJAU NE MODEM Nikji Ni pep lijia' nabe´en tijo kiñu´u havu se ndanú nabe´en jiuk tiji kipíei (PIN): manajap makju´ngun´iang vuju nikji Rabe'en se nama katsjau Lij'i vupa'i lichjau viñkjiu software kue´e lichjau ne valet matsjaú re ndichu´malejeik ne me´eu vatsau´ne matung majau k´ua nan´iu´up viñkjiu vakjá stiñkjia´mep manú Kad-ua niñdchaul' pu katsjau nkjai pu kily´i kimijiu vanjiu´pu lét kuvu skad-akatsjau lana´at tanjú, maiñ se nip sanda nikji kjé Kauts' se tibiaí pep kuljeín kunju' vivium ne ngube´ei vujunkji maiñ kiun´ne manup ngun´iang matsjau viñkjiu%s... ngunjiu´ pu se vake: ngube´eí Lem puse vupai nikji nip chipía lani´at lamejep %s... latsjau viñkjiu lani´at lamejep %s... lamei viñkjiu tumeí nanjiul' nda ngulju' vake´ma nata´au sad-a´seli´iajaun´likiu´uch ne kueé ma namajep ne nikji nixi´i (3G) ne nikji nda´ajaut lét pu valei matsjau lichja viñkjiu tanee:     %s tanee:   color=%s hue=%s nangau:   %s nangau:   color=%s hue=%s 