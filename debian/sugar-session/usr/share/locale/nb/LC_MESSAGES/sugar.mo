��    k      t  �   �       	     !	  
   8	     C	     L	     ]	     f	     m	     v	     ~	  3   �	     �	  
   �	     �	     �	     �	     �	     
     
     
     %
     6
     M
     i
     o
  3   }
     �
  	   �
     �
     �
     �
     �
     �
  
   �
                      +   !     M     h     �     �     �     �     �     �     �     �  	   �     �     �  	   �     �     �     �       
             +  
   4     ?     E     R     Z     _  
   g     r     �  	   �     �  
   �  	   �  	   �     �     �     �     �     �     �                         )     1     ?     H     X     a     |     �     �     �     �     �     �     �     �     �     �     �               0  Z   6  3   �  �  �     V     n     z     �     �     �     �     �     �  1   �                    %     ,     =     V     \     e     k     x     �     �     �  3   �  	   �  	                  '     3     :  	   G     Q     Z     a     f  :   l     �     �     �     �     �  	   �     �     
                     -     4     ;     H     Q     b     s     x     �  
   �     �     �     �     �     �  
   �     �     �     �  	     
             $     -     6     =  	   K     U  
   d     o     w  	   �     �     �     �     �     �     �     �      �     �  
   �               "     /     8     >     J     h  	   q     {     �     �     �  T   �  ;           k   @      a       
   ]       K   U   S       g   d       F                  R           \   +   <         C              #                    )       X   N         J          h   7          %   8   ,       c   j              V   &   L   !      5       B   9   Y   P   4          `   $          D   '   .   O   G   2   b       :              *   "   H   ^   e   3              -   Z   >   _   W   I   E   ?   T       =   Q             (          [       A       f   	           i          1   6   ;                                0               M   /          %(free_space)d MB Free %s seconds About Me Activation Delay Activity Anyone Anything Anytime Authentication Type: Automatic power management (increases battery life) Back Background Build: Cancel Cancel changes Changes require restart Channel Charged Charging Choose an object Click to change color: Click to change your color: Close Confirm erase Confirm erase: Do you want to permanently erase %s? Connect Connected Connecting... Corner Date & Time Decline Description: Disconnect Disconnected Done Edge Erase Error in automatic pm argument, use on/off. Error in specified colors. Error timezone does not exist. Frame Group Home Identity Invite to %s Join Journal Keep Key Type: Language Later List view Make friend Mesh Network %d Mesh Network %s Mute My Battery My Speakers My class My friends Name: Neighborhood Network Next No date No preview Not available Open Open with Participants: Past month Past week Past year Power Power management Register Remove favorite Remove friend Restart Restart now Resume Ring Serial Number: Server: Show contents Shutdown Since yesterday Software Sorry I do not speak '%s'. Start Starting... State is unknown. Stop Tags: Timezone Today Untitled Value must be an integer. Warning Wireless You must enter a name. Your Journal is empty Your Journal is full never sugar-control-panel: WARNING, found more than one option with the same name: %s module: %r sugar-control-panel: key=%s not an available option Project-Id-Version: sugar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-06-18 19:42+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: Norsk bokmål <i18n-no@lister.ping.uio.no>
Language: nb
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.0.5
 %(free_space)d MB ledig %s sekunder Om meg Aktiveringsforsinkelse Lek Hvem som helst Hva som helst Når som helst Autentiseringsform: Automatisk strømstyring (øker batterilevetiden) Tilbake Bakgrunn Bygg: Avbryt Avbryt endringer Endringer krever omstart Kanal Oppladet Lader Velg en ting Klikk her for å endre farge Klikk for å endre din farge: Lukk Bekreft sletting Bekreft sletting: Ønsker du å slette %s for godt? Koble til Tilkoblet Kobler til... Hjørne Dato og tid Avslå Beskrivelse: Koble fra Avkoblet Ferdig Kant Slett Feil i argument til automatisk strømstyring, bruk on/off. Feil i de angitte fargene. Feil: Tidssonen finnes ikke. Ramme Gruppe Hjem Identitet Inviter til %s Bli med Dagbok Behold Nøkkelform: Språk Senere Listevisning Bli venn Maskenettverk %d Maskenettverk %s Demp Mitt Batteri Mine Høytalere Min klasse Mine venner Navn: Nabolag Nettverk Neste Ingen dato Ingen forhåndsvisning Ikke tilgjengelig Åpne Åpne med Deltakere: Sist måned Sist uke Sist år Strøm Strømstyring Registrer Fjern favoritt Fjern venn Omstart Start om nå Gjenoppta Ring Serienummer: Tjener: Vis innhold Slå av Siden igår Programvare Beklager, jeg snakker ikke '%s'. Start Starter... Tilstanden er ukjent. Stans Merkelapper: Tidssone I dag Uten tittel Verdien må være et heltall. Advarsel Trådløs Du må skrive inn et navn. Dagboken din er tom Dagboken din er tom aldri sugar-control-panel: ADVARSEL, fant mer enn en opsjon med samme navnet: %s modul: %r sugar-control-panel: key=%s er ikke en tilgjengelig opsjon. 