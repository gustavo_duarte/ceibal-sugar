��            )         �     �     �     �     �  
   �     �     �     �     �                 
        %     -  
   A     L  
   Z  	   e  	   o  B   y     �     �     �     �     �     �     �     �       �  &  
   �  
   �     �     �     �     �                          '     0     <     K  -   W     �     �     �     �     �  M   �           -  	   ?     I     P     \     a     j     �                
                          	                                                                                              Anyone Anything Anytime Back Background Cancel Choose an object Close Description: Journal Keep My class My friends No date No matching entries No preview Participants: Past month Past week Past year Please delete some old Journal entries to make space for new ones. Resume Show Journal Since yesterday Start Tags: Today Untitled Your Journal is empty Your Journal is full Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-06-18 19:57+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Pootle 2.0.5
 Ktokoľvek Čokoľvek Kedykoľvek Späť Pozadie Zrušiť Vybrať objekt Zavrieť Popis: Denník Udržať Moja trieda Moji kamaráti Bez dátumu Neboli nájdené žiadne vyhovujúce záznamy Bez náhľadu Spolupracovníci: Minulý mesiac Minulý týždeň Minulý rok Prosím vymaž staré záznamy z denníka, aby sa uvoľnilo miesto pre nové. Pokračovať Zobraziť denník Od včera Štart Označenie: Dnes Bez mena Tvoj denník je prázdny Tvoj denník je plný 