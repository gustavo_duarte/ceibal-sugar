��    <      �  S   �      (     )     2     ;     B     K     S     h  
   m     x          �     �     �     �     �  #   �     �  -        0     O     U     Z     g     l     t  	   y  -   �     �     �     �     �     �  
   �     �                 
        )     ,  
   1  	   <  	   F     P     Y     g  
   n     y     �     �     �     �     �     �     �  1   �                 }  4     �	     �	     �	  
   �	     �	     �	     
     
     &
  
   2
  )   =
     g
     s
     �
     �
  7   �
     �
  W   �
  I   D     �     �     �     �     �     �     �  4   �     -     C     ^     |  
   �     �     �     �     �     �     �               $     6     J     \     |     �     �     �     �  7   �               <     [     d  p   s  
   �     �  %              8   :           4       0                                       <   -   6         #               1   3         .               /           ,       (         2   !       "   7          *   ;   '       
   $          +          &   	                 )      9   5                %                 About Me Activity Anyone Anything Anytime Authentication Type: Back Background Cancel Channel Click to change color: Description: Disconnected Done Erase Error in specified color modifiers. Error in specified colors. Error in specified radio argument use on/off. Error timezone does not exist. Group Home Invite to %s Join Journal Keep Key Type: Language for code=%s could not be determined. Make friend Mesh Network Mesh Network %d Mesh Network %s My class My friends Name: Neighborhood Next No date No preview Ok Open Past month Past week Past year Register Remove friend Resume Screenshot Shutdown Since yesterday Sorry I do not speak '%s'. Start Starting... State is unknown. Stop Tags: To apply your changes you have to restart Sugar.
 Today Untitled You must enter a name. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-08-30 17:42+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Pootle 2.0.5
 درباره ی من  فعالیت هرکس هرچیز هرزمان نوعیت تصدیق: عقب پس‌زمینه لغو کن کانال برای تبدیلی رنگ تیک کن: تعریف: منقطع شد شد پاک کن اشتباه در تعدیل کننده رنگ معین اشتباه د رنگ معین اشتباه دراستدلال معین از خاموش/روشن نمودن رادیو اشتباه منطقه جغرافیایی ساعات وجود ندارد گروه خانه دعوت به ( ) %s پیوستن روزنگار نگه داشتن نوعیت کلید: زبان برای رمز=%s تعین نمی گردد دوست بسازید شبکه بافته شده شبکه بافته شده %d شبکه بافته شده %s کلاسم دوستانم نام: همسایگی بعدی بی تاریخ پیش‌نمایشی نیست درست است. باز کردن ماه گذشته هفته گذشته سال گذشته راجستر و ثبت کردن دوست را پاک کن از سر گرفتن عکس صفحه بند کردن از دیروز ببخشید من سخن گفته نمیتوانم "%s" شروع در حال شروع شدن.... حالان نامشخص هست توقف برچسبها برای اجرا نمودن تغیرات تان شما باید شوگر را دوباره چالان کنید
 امروز بی‌عنوان باید یک اسم وارد کنی! 