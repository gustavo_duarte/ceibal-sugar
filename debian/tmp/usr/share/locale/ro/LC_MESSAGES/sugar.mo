��    =        S   �      8  
   9     D     M     T     ]     e     z  
        �     �     �  3   �     �     �     �       #   	     -  -   H     v     �     �     �     �     �     �  	   �  -   �     �                     0  
   9     D     J     W     \  
   d     o     }     �  
   �  	   �  	   �     �     �     �  
   �     �     �     �                    %     *  1   0     b     h  �  q  
   '
  
   2
     =
     E
     K
     T
     g
     o
  	   v
     �
     �
  >   �
  
   �
  
   �
     �
        2         ;  >   \     �     �     �     �     �     �     �  
   �  -   �     -     =     K     \  	   m     w     �     �     �     �     �     �     �     �     �     �               %     5     :     I     R  $   Z          �     �     �  	   �  <   �     �     �           :               )                    =                             /   8         $                3   5         0               1           .      *         4   "   !   #   9          ,   6   (   2   
   %          -          '   	                 +      ;   7                &       <          %s seconds Activity Anyone Anything Anytime Authentication Type: Back Background Cancel Channel Click to change color: Could not access ~/.i18n. Create standard settings. Description: Disconnected Done Erase Error in specified color modifiers. Error in specified colors. Error in specified radio argument use on/off. Error timezone does not exist. Group Home Invite to %s Join Journal Keep Key Type: Language for code=%s could not be determined. Make friend Mesh Network Mesh Network %d Mesh Network %s My class My friends Name: Neighborhood Next No date No preview Not available Ok Open Past month Past week Past year Register Remove friend Resume Screenshot Shutdown Since yesterday Sorry I do not speak '%s'. Start Starting... State is unknown. Stop Tags: To apply your changes you have to restart Sugar.
 Today Untitled Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-06-18 19:54+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < 20)) ? 1 : 2);;
X-Generator: Pootle 2.0.5
 %s secunde Activitate Oricine Orice Oricând Tip autentificare: Înapoi Fundal Anulează Canal Clic pentru a schimba culoarea: Nu s-a putut accesa ~/.i18n. Se crează configurări standard. Descriere: Deconectat Gata Șterge Eroare la specificarea modificatorului de culoare. Eroare la culoarea specificată. Eroare la argumentul radio specificat. Folosiţi pornit/oprit. Eroare, fusul orar nu există. Grup Acasă Invită la %s Alătură-te Jurnal Păstrează Tip cheie: Nu s-a putut determina limba pentru codul %s. Adaugă prieten Reţea plasă Reţea plasă %d Reţea plasă %s Clasa mea Prietenii mei Nume: Vecinătate Următor Fără dată Fără previzualizare Indisponibil Ok Deschide De luna trecută De săptămâna trecută De anul trecut Înregistrează Şterge prieten Reia Captură ecran Opreşte De ieri Îmi pare rău, nu vorbesc „%s”. Start Se porneşte... Stare necunoscută. Oprește Etichete: Pentru a activa modificările trebuie să reporniţi Sugar.
 Azi Fără titlu 