��    v      �  �   |      �	     �	     �	     �	      
     
     
     
     #
     ,
     >
     E
     N
     U
     ^
     f
  
   k
     v
     z
     �
     �
     �
     �
     �
     �
     �
                 3   #     W  	   _     i     w     �     �  
   �     �     �     �     �     �     �     �     �     �     �  	   �     �     �  	                       &  
   /     :     @     M     R  
   Z     e     s     v  	   {     �  
   �  	   �  	   �     �     �     �     �     �     �               "     *     6     =     I     N     V     ^     k     t     �     �     �     �     �     �  
   �     �     �     �     �     �     �     �               '     .  
   7  	   B     L     Y     e     u     �     �  /   �     �     �     �       �       �     �     �     �     �     �     �     �     �  	   �                    !     *     0  	   8     B     J     i     q     }     �     �     �     �     �     �  >      	   ?  
   I     T     e     r     ~     �     �  	   �     �     �     �     �  
   �     �     �     �     �     �          
  	        &     -     <     I     Y     a     h     p     y  	   �     �  	   �     �     �     �     �     �     �     �     �     �                ?     R     d     p     �     �     �     �     �     �     �     �     �     �     �     �            	             /     ?     H  	   O     Y     ^     k     y     �     �     �     �     �     �     �     �       
   "  1   -     _     w     �     �     U       f   h   T   !          Q   i   n   1          *      5   F   >   <   k      [      #   +       .   p   G       &       L       =   "       2   Y   P   q       e      X   V              0      t   \   8   6   c       N   /   R              :                      S   I       W      %   s       '   J       ,   B   b      E         K   a      M           O             ]       ^       u   @   m                              D   ?      j   g   _   v   3   ;   H   7       (   r       
   A              Z       9   4   C   )              $           	                 d      o   `   l   -    %d KB %dB %dKB %dMB %s clipping %s of %s <Ctrl>%d About Me About my Computer Accept Activity Anyone Anything Anytime Back Background Box Cancel Cannot connect to the server. Charged Charging Choose an object Clear search Click to change color: Click to change your color: Close Collaboration Confirm erase Confirm erase: Do you want to permanently erase %s? Connect Connected Connecting... Date & Time Decline Description: Disconnect Done Erase Freeform Full license: Group Home Identity Join Journal Keep Key Type: Language Later List view Logout Mute My Settings My class My friends Name: Neighborhood Next No date No preview Not available Ok Open Open with Participants: Past month Past week Past year Power Power management Radio Register Registration Failed Registration Successful Remove favorite Remove friend Restart Restart now Resume Resume with Ring Send to Server: Show Journal Shutdown Since yesterday Software Software Update Sound Muted Source Spiral Start Start with Starting... State is unknown. Stop Sugar: Tags: Today Transfer from %s Transfer to %s Triangle Unmute Untitled User Color User Name View Details View Source View source: %r View source: %s Volume Level Warning You are now registered with your school server. You must enter a name. Your Journal is empty Your Journal is full never Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-08-30 15:57+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Pootle 2.0.5
 %d KB %dB %dKB %dMB %s výstřižku %s z %s <Ctrl>%d O Mě Můj Počítač Přijmout Činnost Kdokoliv Cokoliv Kdykoliv Zpět Pozadí Schránka Zrušit Nemohu se připojit k serveru. Nabité Nabíjí se Vybrat předmět Smazat nalezené Kliknout pro změnu barvy: Kliknout pro změnu barvy: Zavřít Spolupráce Potvrdit odstranění Potvrdit odstranění: Opravdu to chceš odstranit navždy %s? Připojit Připojeno Připojuji se... Datum & Čas Nepřijmout Popis: Odpojit Hotovo Odstranit Volný tvar Úplné povolení: Skupina Domů Totožnost Přidat Deník Uchovat Druh Klíče: Jazyk Později Zobrazit pořadí Odhlásit Tichý Moje Dokumenty Moje třída Moji přátelé Jméno: Okolí Další Bez data Bez náhledu Neplatné Ok Otevřít Otevřít s Účastníci: Minulý měsíc Minulý týden Minulý rok Síla Síla vedení Radio Registrovat se Registrace selhala Registrace proběhla úspěšně Vyjmout oblíbené Vyjmout přítele Restartovat Nyní restartovat Začít znovu Začít znovu s Kruh Poslat Server: Ukázat Deník Vypnout Včera Software Aktualizovat Software Zvuk ztišen Zdroj Spirála Spustit Spustit s Spouští se... Neznámý stav. Zastavit Sugar: Vysačky: Dnes Přenos z %s Přenos do %s Trojúhelník Nahlas Neoznačený Uživatelská Barva Uživatelské Jméno Zobrazit Podrobnosti Zobrazit Zdroj Zobrazit zdroj: %r Zobrazit zdroj: %s Úroveň hlasitosti Varování Nyní jseš zaregistrovaný na školním serveru. Musíš vložit jméno. Tvůj Deník je prázdný Tvůj Deník je plný nikdy 