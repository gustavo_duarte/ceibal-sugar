��    F      L  a   |              
        )     2     D  3   M  
   �     �     �     �     �     �     �     �     �  	   �     �          !     (     4     9     ?     Z     y  	   �     �     �     �     �     �     �     �     �  -   �            
         +     7     =     E     S     d  
   l     w     �     �     �     �     �     �  �   �     x	  0   �	     �	     �	     �	     �	     �	     
     
     0
     E
     R
     l
     z
     �
     �
  �  �
     ,     J     X     a       H   �     �     �     �     �            0   4     e     n  
   �     �  (   �     �     �     �  
   �  0   �  *   &     Q     n  
   z     �  
   �     �     �     �     �     �  2   �     
     '     0     D     \     b     i     w     �     �     �     �     �  
   �  #   �            �   7     �  _        d  -   z     �     �     �     �          "     4  %   G     m     v     �  !   �     D   B                  )   /   
      !          '      8   .                        5   +          >      0   6      2               E           :   &         F         A      ,   3              -              	   @      C                        ;   9              7   4           ?   1   $           (          <   *         %      =      "      #        %(hour)d:%(min).2d remaining %s seconds <Ctrl>%d About my Computer Activity Automatic power management (increases battery life) Background Build: Channel Charged Charging Click to change color: Click to change your color: Close Collaboration Connected Connecting... Copyright and License Corner Date & Time Edge Error Error in specified colors. Error timezone does not exist. Favorites view %d Firmware: Frame Full license: Group Home IP address: %s Identity Journal Language Language for code=%s could not be determined. Mesh Network %d Mute My Battery My Speakers Name: Network Not available Power management Removed Screenshot Screenshot of "%s" Serial Number: Server: Software Sorry I do not speak '%s'. Speed State is unknown. The server is the equivalent of what room you are in; people on the same server will be able to see each other, even when they aren't on the same network. Timezone Turn off the wireless radio to save battery life Unmute Value must be an integer. Very little power remaining Wired Network Wireless Wireless Firmware: You must enter a name. Your Journal is full fill:     %s fill:     color=%s hue=%s instantaneous never stroke:   %s stroke:   color=%s hue=%s Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-08-30 17:21+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: he
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.0.5
 נותרו %(hour)d:%(min).2d %s שניות <Ctrl>%d קצת על המחשב שלי פעילות ניהול חשמל אוטומטי (מאריך את חיי הסוללה) רקע בנייה: ערוץ הסוללה מלאה בטעינה לחץ כדי לשנות צבע יש ללחוץ כדי לשנות את הצבע: סגור שיתוף פעולה מחובר מתחבר... זכויות יוצרים ורישיון פינה תאריך וזמן קצה שגיאה אירעה שגיאה בצבעים שהוזנו. שגיאה, אזור זמן לא קיים. תצוגת מעודפים %d קושחה: מסגרת רישיון מלא: קבוצה בית כתובת IP: %s זהות יומן שפה לא נמצאה שפה המתאימה לקוד=%s. רשת מְבֻזָּרת %d השתק הסוללה שלי הרמקולים שלי שם: רשת לא זמין ניהול חשמל נמחק צילום מסך צילום מסך של "%s" מספר סידורי: שרת: תוכנה סליחה, איני דובר '%s'. מהירות מצב אינו ידוע. השרת דומה לחדר שבו אתם נמצאים; אנשים המחוברים לאותו שרת יכולים לראות זה את זה, אפילו אם הם לא על אותה רשת. אזור זמן כדאי לכבות את הרשת האלחוטית כדי להאריך את חיי הסוללה ביטול השתקה הערך חייב להיות מספר שלם. סוללה חלשה מאוד רשת קוית רשת אלחוטית קושחה אלחוטית: עליך להקליד שם. יומנך מלא מילוי:     %s מילוי:     צבע=%s גוון=%s מידי אף פעם ציור:   %s ציור:   צבע=%s גוון=%s 