��    �      �  �   |	      �     �     �  
   �                    +     4     ;     D     L  3   a     �  
   �     �     �     �     �     �     �     �                    &     =     Y     _  3   m     �  	   �     �     �     �     �  3   �          #     +     8  E   P  
   �     �     �     �     �  +   �  #   �       -   )     W     v  	   �     �     �     �     �     �     �     �     �     �     �  	   �     �  -   �     #  	   )     3     A     M     R     _     o       
   �     �     �  
   �     �     �     �     �     �  
   �     �     �     �  	   �       
     	     	   %  B   /     r     x     �     �     �     �     �     �     �     �     �     �  
                  $     1     ?     H     X     a     q     �     �     �     �     �  �   �     �     �  *   �     �  1   �          #     ,     3  �  <     �     �            /   (     X     o     �     �     �     �     �     �     �     �  Z     3   o  �  �  $   (  !   M     o     �     �  0   �     �  
   �     �     �  $     U   1  
   �     �  
   �     �  
   �     �  ;   �  2     
   ;     F     W     j  8   �  8   �     �  6   
  �   A     �     �     �       >        \  n   e     �     �     �  9     �   F     �     �     �     
         K   $   M   p   4   �   \   �   D   P!     �!     �!  
   �!     �!     �!  
   	"     "     !"     8"     K"     h"     w"     �"     �"  I   �"     �"     �"  (   #  &   E#     l#     q#     �#     �#     �#     �#  +   �#     $     $     2$     :$  
   G$     R$     a$     q$     �$     �$     �$     �$     �$     �$     �$     %  �   6%     �%  9   �%  
   &     %&  ,   >&  ,   k&  *   �&  &   �&     �&     '     '     5'     D'     c'     |'  (   �'  0   �'     �'     �'     	(  *   (  0   C(     t(     �(     �(  &   �(     �(  �  �(  $   e*     �*  B   �*     �*  }   �*     s+     |+     �+     �+  0  �+  ;   �-  8   %.     ^.     o.  Y   �.  *   �.  +   /  )   1/     [/  1   v/     �/     �/     �/  '   �/     �/  �   0  D   �0     w   d              I   k   �   �       '   X      .          %   a   `   /   $   ;      	   9   �       ^   ?   |   6       p   W       4   h   G   �       l   S       }   V       �          7   �       q       �   �   t   0   [             N              D       #          y   K   T         "         v       f              C      �   +   J   n   A   E   \       x       �   )   1   
      O          H   o       �       =   -       *   :       �          (       ]   Z               !   �                  �       Y          @   {   �   >   F   B       �       <   5   R          ~   �   �                  Q           �   U   b       &   m   �   3              g               e   2   �   �   _           8      u   �       j      M   z                         s      i   P       ,       L          c   r    %(free_space)d MB Free %(hour)d:%(min).2d remaining %s seconds <Ctrl>%d About Me Activation Delay Activity Anyone Anything Anytime Authentication Type: Automatic power management (increases battery life) Back Background Box Build: Cancel Cancel changes Cannot connect to the server. Changes require restart Channel Charged Charging Choose an object Click to change color: Click to change your color: Close Confirm erase Confirm erase: Do you want to permanently erase %s? Connect Connected Connecting... Copy Copyright and License Corner Could not access ~/.i18n. Create standard settings. Date & Time Decline Description: Discard network history Discard network history if you have trouble connecting to the network Disconnect Disconnected Done Edge Erase Error in automatic pm argument, use on/off. Error in specified color modifiers. Error in specified colors. Error in specified radio argument use on/off. Error timezone does not exist. Favorites view %d Firmware: Frame Freeform Full license: Group Home Identity Invite to %s Join Journal Keep Key Type: Language Language for code=%s could not be determined. Later List view Make favorite Make friend Mesh Mesh Network Mesh Network %d Mesh Network %s Mute My Battery My Speakers My class My friends Name: Neighborhood Network Next No date No preview Not available Ok Open Open with Participants: Past month Past week Past year Please delete some old Journal entries to make space for new ones. Power Power management Radio Register Registration Failed Registration Successful Remove favorite Remove friend Restart Restart now Resume Ring Screenshot Serial Number: Server: Show Journal Show contents Shutdown Since yesterday Software Software Update Sorry I do not speak '%s'. Spiral Start Starting... State is unknown. Stop Sugar is the graphical user interface that you are looking at. Sugar is free software, covered by the GNU General Public License, and you are welcome to change it and/or distribute copies of it under certain conditions described therein. Sugar: Tags: The server could not complete the request. Timezone To apply your changes you have to restart Sugar.
 Today Triangle Unmute Untitled Usage: sugar-control-panel [ option ] key [ args ... ] 
    Control for the sugar environment. 
    Options: 
    -h           show this help message and exit 
    -l           list all the available options 
    -h key       show information about this key 
    -g key       get the current value of the key 
    -s key       set the current value for the key 
    -c key       clear the current value for the key 
     Value must be an integer. Very little power remaining Warning Wireless You are now registered with your school server. You must enter a name. Your Journal is empty Your Journal is full fill:     %s fill:     color=%s hue=%s instantaneous never stroke:   %s stroke:   color=%s hue=%s sugar-control-panel: %s sugar-control-panel: WARNING, found more than one option with the same name: %s module: %r sugar-control-panel: key=%s not an available option Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-08-30 16:12+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Pootle 2.0.5
 %(free_space)d МБ свободно остават %(hour)d:%(min).2d %s секунди <Ctrl>%d За мен Закъснение на показването Дейност Всеки Всичко Произволна дата Тип удостоверяване: Автоматично УЗ (увеличава живота на батерията) Назад Фон Кутия Издание: Отказ Отмяна Не може да се свърже със сървъра. Промените изискват рестарт Канал Заредена Зареждане Избор на обект Натиснете, за да смените цвета: Натиснете, за да смените цвета: Затваряне Потвърждаване на изтриването Потвърждаване на изтриването: желаете ли да бъде изтрито завинаги занятитето "%s"? Свързване Свързан Отделяне... Копиране Права за разпространение и лиценз Ъгъл Отказан е достъп до ~/.i18n. Моля, създайте стандартни настойки. Дата и час Отказване Описание: Изтриване на мрежовата история Изтрийте мрежовата история, ако имате проблеми да се свържете към мрежа Отделяне Без свързаност Затваряне Ръб Изтриване Грешка в автоматично УЗ, използвайте on/off. Грешка в зададените цветови модификатори. Грешка в зададените цветове. Грешка в зададения аргумент, използвайте вкл/изкл. Грешка, времевата зона не съществува. Любими занятия %d Микропрограма: Рамка Разхвърлян Пълен лиценз: Група Начало Идентичност Покани в %s Присъединяване Дневник Запазване Тип ключ: Език Не може да бъде определен езикът с код=%s. По-късно Списъчен изглед Добавяне към любимите Създаване на приятел Mesh Mesh мрежа Mesh мрежа %d Mesh мрежа %s Заглушаване Моята батерия Моите високоговорители Моят клас Моите приятели Име: Съседи Мрежа Следващ Без дата Без преглед Не е наличен Ок Отваряне Отваряне с Участници: Миналият месец Миналата седмица Миналата година Моля, изтрийте някои от старите записи в дневника, за да освободите място за нови. Захранване Управление на захранването (УЗ) Радио Регистриране Проблем при регистрация Регистрацията е успешна Премахване от любимите Изтриване на приятел Рестартиране Рестартиране Продължаване Пръстен Снимка на екрана Сериен номер: Сървър: Показване на дневника Показване на съдържанието Изключване От вчера Софтуер Обновяване на софтуера Съжалявам, но не говоря '%s'. Спирала Пускане Стартиране... Непознато състояние. Стоп Sugar е графичния интерфейс който виждате. Sugar е свободен софтуер, разпространяван под GNU General Public License, и Вие можете свободно да го променяте и/или разпространявате негови копия при определени условия описани в лиценза. Графична среда (Sugar): Етикети: Сървъра не може да изпълни заявката. Времева зона Моля, рестартирайте графичната среда, за да влязат в сила промените.
 Днес Триъгълник Пускане Без заглавие Използване: sugar-control-panel [ опции ] ключ [ аргументи ... ] 
    Управление на работната среда. 
    Опции: 
    -h           показване на помоща 
    -l           списък на всички опции 
    -h ключ       показване на информация за ключа 
    -g ключ       прочитане на текущата стойност на ключа 
    -s ключ       задаване на нова стойност на ключа 
     Стойността трябва да бъде число. Останала е много малко мощност Внимание Безжична Вече сте регистрирани при Вашия училищен сървър. Трябва да въведете име. Вашият дневник е празен Вашият дневник е пълен запълване:     %s запълване:     цвят=%s нюанс=%s веднага никога очерк:   %s очерк:   цвят=%s нюанс=%s sugar-control-panel: %s sugar-control-panel: ВНИМАНИЕ, намерени са няколко опции с еднакво име: %s модул: %r sugar-control-panel: ключ=%s - непозволена опция 