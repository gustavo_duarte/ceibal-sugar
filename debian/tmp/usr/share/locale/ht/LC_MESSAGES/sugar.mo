��    �      �  �   �      �     �       
   -     8     A     J     [     d     k     t     |  3   �     �     �     �     �     �               %     -     6     G     ^     z     �     �  3   �     �  	   �     �     �  3   �     +     7     @     H  
   U     `     m     r     w  +   }  $   �  #   �     �  -        ;     Z  	   l     v     |     �     �     �     �     �     �     �  	   �     �  -   �     �  	   �     	          #     (     5     E     U  
   Z     e     q  
   z     �     �     �     �     �     �  
   �     �     �     �  	   �     �  
   �  	     	                  0     9     M     e     u     �     �     �     �  
   �     �     �     �     �     �     �     �               -     3     ?     Q     V     ]  *   c     �     �     �     �     �  �  �     [     u     �     �  /   �     �     �     �          !     ;     I     O     \     v  Z   �  3   �  �       �     �  	   �     �     �     �               !     /     ;  6   R     �     �     �     �      �     �     �  	   �                    6     O     V     c  .   y     �     �     �     �  1   �  
   �     
            	   &  	   0     :     ?     C  I   I  (   �  )   �     �  6        ;     V     e  
   u     �     �     �     �     �     �     �     �     �     �     �     �     �            	   (  	   2     <     I     V     g     o  	   |  
   �     �     �     �     �  
   �     �     �     �     �     �     �                    *     6     C     U     \     t     �     �     �     �     �     �     �     �     �               &     -     6     >     U     o  
   w     �     �     �     �  '   �  	   �     �     �     �         	     $      ;   
   T      _   &   g      �      �      �      �      �      �      �      �      !      !  T   :!  4   �!     �   T   B       E   %       Z       .   l              :   �   5           z       g   �   i       -   c              '       =   C       ,   q   /   b       +   n             Q      �   �           s   �              S   O   V   y   �      H   �   k          }   &   K                  �       �   @      M   p   ;   U      6   d   |      I       7   3   �      (                 
          Y           j   [       x       2   \      �       9   F       m   h   8          0   _   {      r       !      "       L   u   J       o          N   P   $   X                  )   �      <   v   A       D          4           	      G   W   >   #   ?   �   a      R      ]   `       w       t   e                 *   ^   f   ~   �   1    %(free_space)d MB Free %(hour)d:%(min).2d remaining %s seconds <Ctrl>%d About Me Activation Delay Activity Anyone Anything Anytime Authentication Type: Automatic power management (increases battery life) Back Build: Cancel Cancel changes Cannot connect to the server. Changes require restart Channel Charged Charging Choose an object Click to change color: Click to change your color: Close Collaboration Confirm erase Confirm erase: Do you want to permanently erase %s? Connect Connected Connecting... Corner Could not access ~/.i18n. Create standard settings. Date & Time Date: %s Decline Description: Disconnect Disconnected Done Edge Erase Error in automatic pm argument, use on/off. Error in specified argument use 0/1. Error in specified color modifiers. Error in specified colors. Error in specified radio argument use on/off. Error timezone does not exist. Favorites view %d Firmware: Frame Freeform Group Home Identity Invite to %s Join Journal Keep Key Type: Language Language for code=%s could not be determined. Later List view Make favorite Make friend Mesh Mesh Network Mesh Network %d Mesh Network %s Mute My Battery My Speakers My class My friends Name: Neighborhood Network Next No date No matching entries No preview Not available Ok Open Open with Participants: Past month Past week Past year Power Power management Register Registration Failed Registration Successful Remove favorite Remove friend Restart Restart now Resume Ring Screenshot Serial Number: Server: Show Journal Show contents Shutdown Since yesterday Software Software Update Sorry I do not speak '%s'. Speed Starting... State is unknown. Stop Sugar: Tags: The server could not complete the request. Timezone Today Triangle Unmute Untitled Usage: sugar-control-panel [ option ] key [ args ... ] 
    Control for the sugar environment. 
    Options: 
    -h           show this help message and exit 
    -l           list all the available options 
    -h key       show information about this key 
    -g key       get the current value of the key 
    -s key       set the current value for the key 
    -c key       clear the current value for the key 
     Value must be an integer. Very little power remaining Warning Wireless You are now registered with your school server. You must enter a name. Your Journal is empty Your Journal is full fill:     %s fill:     color=%s hue=%s instantaneous never stroke:   %s stroke:   color=%s hue=%s sugar-control-panel: %s sugar-control-panel: WARNING, found more than one option with the same name: %s module: %r sugar-control-panel: key=%s not an available option Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-31 00:33-0400
PO-Revision-Date: 2013-08-30 17:22+0200
Last-Translator: Chris <cjl@laptop.org>
Language-Team: LANGUAGE <LL@li.org>
Language: ht
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n !=1);
X-Generator: Pootle 2.0.5
 %(free_space)d MB vid %(hour)d:%(min).2d ki rete %s segond <Ctrl>%d Enfòmasyon pèsonèl Reta aktivasyon Aktivite Nenpòt moun Nenpòt bagay Nenpòt lè Kalite otantifikasyon: automatic power management(ogmante tan batri a ap bay) Retounen Bati: Anile Anile chanjman yo Pa kapab konekte nan sèvè a. Redemare pou aplike chanjman yo Chanèl Fin chaje Ap chaje chwazi yon bagay Klike pou chanje koulè: Klike pou chanje koulè: Fèmen Kolaborasyon Konfime sa ou efase a Konfime sa ou efase a: ou vle retire'l net %s? Konekte Konekte An koneksyon... Kwen Aksè enposib ~/.i18n. Kreye paramèt pa defo yo. Dat ak lè Dat: %s Refize Deskripsyon: Dekonekte Dekonekte Fini Bò Efase Agiman pou jere alimantasyon otomatik la pa kòrèk sèvi ak limen/etenn. Agiman espesifye-a pa kòrèk sèvi 0/1. Erè nan modifikatè koulè espesifye yo. Erè nan koulè espesifye yo. Agiman radio espesifye-a pa kòrèk sèvi limen/etenn. Erè lè lokal pa egziste. Fas prefere %d Mikrolojisyèl: Ankadreman Fòm lib Gwoup Lakay Idantite Envite nan %s Rejwen'n Jounal Kenbe kalite kle: Lang Lang pou kod=%s pa ka tèmine. Pi ta Gade lis Ajoute nan favori Fè zanmi ak moun sa Rezo maye Rezo maye Rezo maye %d Rezo maye %s Dezaktive son an Batri-m Opalè m' yo Klas mwen Zanmi m yo Non: Vwazinaj Rezo Pwochen Pa gen dat Pa gen antre ki matche Pa gen previziyalizasyon Pa disponib Ok Ouvri Ouvri ak Patisipan yo: Mwa pase Semèn pase a Lane pase a Alimantasyon Jere alimantasyon Enskri enskripsyon an pa fèt enskripsyon an fèt Siprime favori Retire nan zanmi mwen yo Redemare Redemare kounye a menm Repwann Zanno Ekran projektwa Nimero seri: Sèvè: Montre jounal la Afiche kontni yo Eten'n Depi Yè Pwogram Mete logisyèl la ajou Dezole mwen pa pale '%s'. Vitès Demaraj... Yo pa konnen eta l'. Stope, rete Sugar: Etikèt: Sèvè a pa kapab konplete rekèt la Lè lokal Jodia Triyang Aktive son an San tit Sèvi: Sugar-Kontwòl-panèl [opsyon] kle [args ... ]
	Kontwòl pou anvironman sugar. 
	opsyon: 
	-h	montre mesaj èd sa epi soti 
	-l	fè lis tout opsyon disponib yo 
	-h kle	montre enfòmasyon sou kle sa 
	-g kle	pwan valè kouran kle-a 
	-s kle	fikse valè Kouran pou kle-a 
     Valè-a dwe yon antye. Yon ti kras chaj ki rete Avètisman San fil ou pa enskri avek sèvè lekol ou a. Ou dwe rantre yon non. Jounal ou vid Jounal ou plen fill:     %s fill:     koulè=%s hue=%s imediat Jamè stroke:   %s stroke:   koulè=%s hue=%s sugar-kontwòl-panèl: %s Sugar-control-panel: Atansyon, yo jwenn plizyè opsyon ak yon menm non: %s modil: %r Sugar-kontwòl-panèl: kle=%s pa yon opsyon disponib 